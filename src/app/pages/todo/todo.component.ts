import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { TodoService } from "./services/todo.service";

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.sass'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TodoComponent implements OnInit {

  constructor(
    private readonly todoService: TodoService
  ) { }

  ngOnInit(): void {
  }

  get todoList$() {
    return this.todoService.todoList$
  }


  handleInputSubmit(event: string) {
    this.todoService.addTodo(event)
  }

}
