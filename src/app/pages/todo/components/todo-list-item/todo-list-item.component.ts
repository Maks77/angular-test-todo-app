import { Component, ChangeDetectionStrategy, Input, Output, EventEmitter } from '@angular/core';
import { TodoModel } from "../../../../store/todo/model/todo.model";

@Component({
  selector: 'app-todo-list-item',
  templateUrl: './todo-list-item.component.html',
  styleUrls: ['./todo-list-item.component.sass'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TodoListItemComponent {
  @Input() item: TodoModel
  @Input() showSelectCheckBox = false
  @Output() archiveEvent = new EventEmitter<TodoModel>()
  @Output() deleteEvent = new EventEmitter<TodoModel>()
  @Output() completeEvent = new EventEmitter<TodoModel>()

  onCompletePressed() {
    this.completeEvent.emit(this.item)
  }

  onArchivePressed() {
    this.archiveEvent.emit(this.item)
  }

  onDeletePressed() {
    this.deleteEvent.emit(this.item)
  }

}
