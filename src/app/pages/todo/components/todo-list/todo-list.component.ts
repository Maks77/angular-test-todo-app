import { Component, ChangeDetectionStrategy, Input } from '@angular/core';
import { TodoModel } from "../../../../store/todo/model/todo.model";
import { TodoService } from "../../services/todo.service";
import { Store } from "@ngxs/store";
import { ChangeActiveTodoFilterAction } from "../../../../store/todo/actions/change-active-todo-filter.action";
import { TodoFilterModel} from "../../../../store/todo/model/todo-filter.model";
import { TodoFilterValue } from "../../../../store/todo/model/todo-filter-value.enum";

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.sass'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TodoListComponent {
  @Input() todoList: TodoModel[] = []
  selectedFilterTabIndex = 0
  todoFilters: TodoFilterModel[] = [
    { label: 'All', value: TodoFilterValue.ALL },
    { label: 'Pending', value: TodoFilterValue.PENDING },
    { label: 'Archived', value: TodoFilterValue.ARCHIVED },
    { label: 'Completed', value: TodoFilterValue.COMPLETED }
  ]

  constructor(
    private readonly todoService: TodoService,
    private store: Store
  ) { }

  handleChangeTab(activeFilterIndex: number) {
    this.store.dispatch(new ChangeActiveTodoFilterAction(this.todoFilters[activeFilterIndex].value))
  }

  handleDeleteTodo(todo: TodoModel) {
    this.todoService.deleteTodoItem(todo)
  }

  handleArchiveTodo(todo: TodoModel) {
    this.todoService.archiveTodoItem(todo)
  }

  handleCompleteTodo(todo: TodoModel) {
    this.todoService.changeCompleteStatus(todo, true)
  }

  trackByFn(index: number, item: TodoModel) {
    return item.id;
  }
}
