import { Component, ChangeDetectionStrategy, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-todo-input',
  templateUrl: './todo-input.component.html',
  styleUrls: ['./todo-input.component.sass'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TodoInputComponent {
  @Output() submit = new EventEmitter()
  value = ''

  clearValue(): void {
    this.value = ''
  }

  onEnterPressed() {
    this.submit.emit(this.value)
    this.clearValue()
  }

  onEscPressed() {
    this.clearValue()
  }

}
