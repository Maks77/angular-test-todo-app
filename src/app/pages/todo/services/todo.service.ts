import { Injectable } from '@angular/core';
import { TodoModel } from "../../../store/todo/model/todo.model";
import { Observable } from "rxjs";
import { Select, Store } from "@ngxs/store";
import { AddTodoAction } from "../../../store/todo/actions/add-todo.action";
import { RemoveTodoAction } from "../../../store/todo/actions/remove-todo.action";
import { TodoState } from "../../../store/todo/state/todo.state";
import { ArchiveTodoAction } from "../../../store/todo/actions/archive-todo.action";
import { ChangeTodoCompleteStatusAction } from "../../../store/todo/actions/change-todo-complete-status.action";

@Injectable({
  providedIn: 'root'
})
export class TodoService {
  @Select(TodoState.items$) public todoList$: Observable<TodoModel[]>

  constructor(
    private store: Store
  ) { }


  addTodo(todoText: string): void {
    const newTodoItem: TodoModel = {
      id: Date.now().toString(),
      text: todoText,
      isCompleted: false,
      isArchived: false
    }
    this.store.dispatch(new AddTodoAction(newTodoItem))
  }

  deleteTodoItem(todo: TodoModel): void {
    this.store.dispatch(new RemoveTodoAction(todo))
  }


  archiveTodoItem(todo: TodoModel): void {
    this.store.dispatch(new ArchiveTodoAction(todo))
  }

  changeCompleteStatus(todo: TodoModel, isCompleted: boolean): void {
    this.store.dispatch(new ChangeTodoCompleteStatusAction(todo, isCompleted))
  }
}
