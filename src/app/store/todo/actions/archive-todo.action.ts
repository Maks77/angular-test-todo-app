import { TodoModel } from "../model/todo.model";

export class ArchiveTodoAction {
  static readonly type = '[Todo] Archive'
  constructor(public todoItem: TodoModel) {}
}
