import { TodoModel } from "../model/todo.model";

export class AddTodoAction {
  static readonly type = '[Todo] Add'
  constructor(public todoItem: TodoModel) {}
}
