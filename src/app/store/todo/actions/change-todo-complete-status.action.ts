import { TodoModel } from "../model/todo.model";

export class ChangeTodoCompleteStatusAction {
  static readonly type = '[Todo] Change complete status'
  constructor(
    public todoItem: TodoModel,
    public isCompleted: boolean
  ) {}
}
