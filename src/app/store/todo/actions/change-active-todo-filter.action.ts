import { TodoFilterValue } from "../model/todo-filter-value.enum";

export class ChangeActiveTodoFilterAction {
  static readonly type = '[Todo] change active todo filter'
  constructor(public activeFilterValue: TodoFilterValue) {}
}
