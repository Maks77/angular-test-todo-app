import { TodoModel } from "../model/todo.model";

export class RemoveTodoAction {
  static readonly type = '[Todo] remove'
  constructor(public todo: TodoModel) {}
}
