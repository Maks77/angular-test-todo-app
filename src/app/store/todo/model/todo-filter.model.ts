import { TodoFilterValue } from "./todo-filter-value.enum";

export interface TodoFilterModel {
  label: string
  value: TodoFilterValue
}

