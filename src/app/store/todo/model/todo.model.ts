export interface TodoModel {
  id: string
  text: string
  isArchived: boolean
  isCompleted: boolean
}
