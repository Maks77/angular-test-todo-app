export enum TodoFilterValue {
  ALL = 'all',
  PENDING = 'pending',
  ARCHIVED = 'archived',
  COMPLETED = 'completed'
}
