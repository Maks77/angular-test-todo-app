import { Action, Selector, State, StateContext } from "@ngxs/store";
import { AddTodoAction } from "../actions/add-todo.action";
import { TodoModel } from "../model/todo.model";
import { RemoveTodoAction } from "../actions/remove-todo.action";
import { ArchiveTodoAction } from "../actions/archive-todo.action";
import { patch, updateItem } from "@ngxs/store/operators";
import { ChangeTodoCompleteStatusAction } from "../actions/change-todo-complete-status.action";
import { ChangeActiveTodoFilterAction } from "../actions/change-active-todo-filter.action";
import { TodoFilterValue } from "../model/todo-filter-value.enum";

export class TodoStateModel {
  items: TodoModel[]
  activeFilter: TodoFilterValue
}


@State<TodoStateModel>({
  name: 'todo',
  defaults: {
    items: [],
    activeFilter: TodoFilterValue.ALL
  }
})
export class TodoState {

  @Selector()
  static items$(state: TodoStateModel) {
    switch (state.activeFilter) {
      case TodoFilterValue.ALL:
        return state.items
      case TodoFilterValue.ARCHIVED:
        return state.items.filter(el => el.isArchived)
      case TodoFilterValue.COMPLETED:
        return state.items.filter(el => el.isCompleted)
      case TodoFilterValue.PENDING:
        return state.items.filter(el => !el.isCompleted)
      default:
        return state
    }
  }

  @Action(AddTodoAction)
  add(ctx: StateContext<TodoStateModel>, action: AddTodoAction) {
    const state = ctx.getState()
    ctx.patchState({
      ...state,
      items: [...state.items, action.todoItem]
    })
  }

  @Action(RemoveTodoAction)
  remove(ctx: StateContext<TodoStateModel>, action: RemoveTodoAction) {
    const state = ctx.getState()
    ctx.patchState({
      ...state,
      items: state.items.filter(item => item.id !== action.todo.id)
    })
  }

  @Action(ArchiveTodoAction)
  archive(ctx: StateContext<TodoStateModel>, action: ArchiveTodoAction) {
    ctx.setState(
      patch({
        items: updateItem((el) => el?.id === action.todoItem.id, {
          ...action.todoItem,
          isArchived: true
        })
      })
    )
  }

  @Action(ChangeTodoCompleteStatusAction)
  changeCompleteStatus(ctx: StateContext<TodoStateModel>, action: ChangeTodoCompleteStatusAction) {
    ctx.setState(
      patch({
        items: updateItem((el) => el?.id === action.todoItem.id, {
          ...action.todoItem,
          isCompleted: action.isCompleted
        })
      })
    )
  }

  @Action(ChangeActiveTodoFilterAction)
  changeActiveFilter(ctx: StateContext<TodoStateModel>, action: ChangeActiveTodoFilterAction) {
    ctx.patchState({
      ...ctx.getState(),
      activeFilter: action.activeFilterValue
    })
  }

}
